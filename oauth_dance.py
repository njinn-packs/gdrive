from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

gauth = GoogleAuth()
# the following is not required as the defaults of the implementation work fine
#gauth.settings['client_config_backend'] = 'settings'
#gauth.settings['client_config'] = {'client_id': client_id, 
#    'client_secret': client_key, 
#    'auth_uri': 'https://accounts.google.com/o/oauth2/auth', 
#    'token_uri': 'https://accounts.google.com/o/oauth2/auth', 
#    'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
#    'revoke_uri': None}
gauth.settings['save_credentials'] = True
gauth.settings['save_credentials_backend'] = 'file'
gauth.settings['save_credentials_file'] = 'credentials.json'
gauth.settings['get_refresh_token'] = True
gauth.settings['oauth_scope'] = ['https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/drive.file', 'https://www.googleapis.com/auth/drive.metadata']

gauth.CommandLineAuth()