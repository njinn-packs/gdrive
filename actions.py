from pydrive.drive import GoogleDrive
from pydrive.auth import GoogleAuth
from oauth2client import client


class GDriveBase():

    def __init__(self):
        self.oauth_connection = None
        self.__gdrive_instance = None

    @property
    def gdrive(self):
        if not self.__gdrive_instance:
            self.__gdrive_instance = self.gdrive_instance()
        return self.__gdrive_instance

    def gdrive_instance(self):
        if self.oauth_connection and self.oauth_connection.get('access_token') \
                and self.oauth_connection.get('client_id') \
                and self.oauth_connection.get('client_key') \
                and self.oauth_connection.get('refresh_token'):

            self.access_token = self.oauth_connection.get('access_token')
            self.client_id = self.oauth_connection.get('client_id')
            self.client_key = self.oauth_connection.get('client_key')
            self.refresh_token = self.oauth_connection.get('refresh_token')

            credentials = f'{{"access_token":"{self.access_token}","client_id":"{self.client_id}","client_secret":"{self.client_key}","refresh_token":"{self.refresh_token}","token_expiry":"2019-04-11T21:37:55Z","token_uri":"https://accounts.google.com/o/oauth2/token","user_agent":null,"revoke_uri":"https://oauth2.googleapis.com/revoke","id_token":null,"id_token_jwt":null, "scopes":["https://www.googleapis.com/auth/drive","https://www.googleapis.com/auth/drive.metadata","https://www.googleapis.com/auth/drive.file"],"token_info_uri":"https://oauth2.googleapis.com/tokeninfo","invalid":false,"_class":"OAuth2Credentials","_module":"oauth2client.client"}}'

            gauth = GoogleAuth()
            gauth.settings['client_config_backend'] = 'settings'
            gauth.settings['client_config'] = {'client_id': self.client_id,
                                               'client_secret': self.client_key,
                                               'auth_uri': 'https://accounts.google.com/o/oauth2/auth',
                                               'token_uri': 'https://accounts.google.com/o/oauth2/auth',
                                               'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
                                               'revoke_uri': None}

            gauth.credentials = client.Credentials.new_from_json(credentials)

            gdrive = GoogleDrive(gauth)

        else:
            raise Exception('No or incomplete connection information provided')

        return gdrive


class CreateFolder(GDriveBase):

    folder_name = 'New Folder'
    target_folder = None
    team_drive_id = None

    def run(self):
        """
        Creates a folder
        """

        result = None

        metadata = {'title': self.folder_name,
                    'mimeType': 'application/vnd.google-apps.folder'}

        if self.target_folder:
            metadata['parents'] = [
                {'kind': "drive#fileLink", 'id': self.target_folder}]

        if self.team_drive_id:
            metadata['teamDriveId'] = self.team_drive_id

        file = self.gdrive.CreateFile(metadata)
        print(f"Creating folder {self.folder_name}")
        if self.team_drive_id:
            print(f"in team drive {self.team_drive_id}")
            file.Upload(param={'supportsTeamDrives': True})
        else:
            file.Upload()

        result = file.metadata

        return {'g_file': result}


class UploadFile(GDriveBase):

    file_path = None
    target_folder = None
    team_drive_id = None

    def run(self):
        """
        Uploads a file while either creating or updating it
        """

        result = None

        file_name = self.file_path.split('/')[-1]
        metadata = {'title': file_name}

        if self.team_drive_id:
            metadata['teamDriveId'] = self.team_drive_id

        if self.target_folder:
            metadata['parents'] = [
                {'kind': "drive#fileLink", 'id': self.target_folder}]

        file = self.gdrive.CreateFile(metadata)
        print(f"Retrieving file content from {self.file_path}")
        file.SetContentFile(self.file_path)

        print(f"Uploading file {file_name} to folder {self.target_folder}")
        if self.team_drive_id:
            print(f"to team drive {self.team_drive_id}")
            file.Upload(param={'supportsTeamDrives': True})
        else:
            file.Upload()

        result = file.metadata

        return {'g_file': result}


class SearchFiles(GDriveBase):

    query = None
    in_path = None
    expect_one = None
    team_drive_id = None

    def find_parent(self, folder_array, root_id=None):
        folder_name = folder_array[0]
        query = f"title = '{folder_name}'"

        if root_id:
            query = f"{query} and '{root_id}' in parents"

        if not self.team_drive_id:
            res = self.gdrive.ListFile({'q': query}).GetList()
        else:
            print(f"in team drive {self.team_drive_id}")
            res = self.gdrive.ListFile({'corpora': 'teamDrive', 'driveId': self.team_drive_id,
                                        'includeItemsFromAllDrives': True, 'q': query, 'supportsAllDrives': True}).GetList()

        if len(res) == 0:
            raise Exception(f"Can't find folder {folder_name}")
        if len(res) != 1:
            raise Exception(f"Found {len(res)} parents, expected to find 1")

        parent = res[0]['id']

        if len(folder_array) > 1:
            return self.find_parent(folder_array[1:], parent)
        else:
            print(f"Found parent {parent}")
            return parent

    def run(self):
        """
        Searches for files matching a query. For details and examples see https://developers.google.com/drive/api/v2/search-files
        """

        res = None

        query = self.query
        if self.in_path:
            parent = None
            parent = self.find_parent(self.in_path.split("/"))
            query = f"{query} and '{parent}' in parents"

        print(f"Searching for \"{query}\"")
        res = self.gdrive.ListFile(
            {'includeItemsFromAllDrives': True, 'q': query, 'supportsAllDrives': True}).GetList()

        g_files = []
        for r in res:
            g_files.append(r.metadata)

        print(f"Found {len(g_files)} match(es)")
        if len(g_files) != 0:
            print(
                list(map(lambda x: {'id': x['id'], 'title': x['title']}, g_files)))

        if self.expect_one == 'True' and len(g_files) != 1:
            raise Exception('Expected exactly one match')

        return {'g_files': g_files}


class DownloadFile(GDriveBase):
    """
    Download file
    """

    file_id = None

    def run(self):
        if not self.file_id:
            raise Exception(f"File ID is required")

        file = self.gdrive.CreateFile({'id': self.file_id})
        print(f"Fetching title for '{self.file_id}'")
        file.FetchMetadata(
            fields='downloadUrl,title,mimeType')

        print(
            f"Downloading file '{self.file_id}' as '{file['title']}'")
        file.GetContentFile(file['title'])

        return {'file': self._njinn.upload_file(file['title'])}


class RetrieveFileContent(GDriveBase):
    """
    Retrieve file content as string
    """

    file_id = None

    def run(self):
        if not self.file_id:
            raise Exception(f"File ID is required")

        file = self.gdrive.CreateFile({'id': self.file_id})
        print(f"Fetching title for '{self.file_id}'")
        file.FetchMetadata(
            fields='downloadUrl,title,mimeType')

        print(
            f"Retrieving file content for '{self.file_id}' aka '{file['title']}'")
        file_content = file.GetContentString()

        return {'file_content': file_content}
